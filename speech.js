export default class Speech {
    constructor(hooks, continueToListen = true) {
        window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
        this.speech = new window.SpeechRecognition()
        this.speech.continueToListen = continueToListen

        this.registerSettings()
        this.registerHooks(hooks)
    }

    /**
     * Recieve text version of spoken words.
     * Gets only first result.
     * 
     * @param {SpeechRecognitionResult} result 
     */
    getTranscript(result) {
        let text = result.results[0][0].transcript
        window.dispatchEvent(new CustomEvent('speech-to-text', { detail: text }))
    }

    /**
     * Default settings for speech recognition.
     */
    registerSettings() {
        this.speech.interimResults = false // Post the result as heard
        this.speech.maxAlternatives = 1 // Post-back only 1 guess
        this.speech.continuous = false // Continue listening
        this.speech.lang = 'en-US' // Default language to check
    }

    registerHooks(hooks = {}) {
        if (Object.entries(hooks).length > 0) return

        this.speech.onresult = this.getTranscript

        // When continuing to speak, result should be reseted
        // and connection should be kept.
        if (!! this.continueToListen) {
            this.speech.onend = () => {
                this.speech.results = {}
                this.speech.stop()
                this.speech.start()
            }
        }
    }

    listen() {
        this.speech.start()
    }
}