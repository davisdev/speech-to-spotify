import Speech from './speech.js'

const speech = new Speech({}, true)

// For now, outputting in console. F12 > Console to see.
speech.listen()

window.addEventListener('speech-to-text', result => console.log(result))